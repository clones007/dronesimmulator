using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasDronePosition : MonoBehaviour
{
    [SerializeField] Transform _dronePosition;
    private Text _positionText;
    // Start is called before the first frame update
    void Start()
    {
        _positionText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _positionText.text = "Drone posiotion: N:"+_dronePosition.position.x+" E: " + _dronePosition.position.z + " Drone Height: " + _dronePosition.position.y;
    }
}
