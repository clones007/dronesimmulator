using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitController : MonoBehaviour
{
    private Rigidbody _myRigidbody;
    private int _hitCounter;
    [SerializeField] Rigidbody _bulletRigidbody;
    [SerializeField] ParticleSystem _hitParticle;
    [SerializeField] ParticleSystem _destroyParticle;

    void Awake()
    {
        _hitCounter = 0;
        _myRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_hitCounter > 6)
        {
            Instantiate(_destroyParticle, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody.tag == "Bullet")
        {
           Instantiate(_hitParticle, collision.GetContact(0).point, transform.rotation);
           _hitCounter++;
        }
    }
}
