using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneSound : MonoBehaviour
{
    private AudioSource _droneSound;
    private float _volume;

    // Start is called before the first frame update
    void Start()
    {
        _volume = 0f;
        _droneSound = GetComponent<AudioSource>();

        _droneSound.Play();
    }

    // Update is called once per frame
    void Update()
    {
        _droneSound.volume = _volume;

        if (DroneMoveController.Get_isOn())
        {
            if (_volume < 1)
            {
                _volume += 0.5f * Time.deltaTime;
            }
        }
        if (!DroneMoveController.Get_isOn())
        {
            if (_volume > 0)
            {
                _volume -= 0.5f * Time.deltaTime;
            }
        }
    }
}
