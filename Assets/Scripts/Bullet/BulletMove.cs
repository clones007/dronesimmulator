using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletMove : MonoBehaviour
{
    private float speed = 100f;

    private void Start()
    {
        Debug.Log("I" + gameObject.name + " exist");
        StartCoroutine(SelfDestruction());
    }

    void Update()
    {
        if (speed != 0)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }


    }
    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }

    IEnumerator SelfDestruction()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
