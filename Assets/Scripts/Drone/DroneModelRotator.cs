using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneModelRotator : MonoBehaviour
{
    private float _rightHorizontal, _rightVertical;

    private Transform _parentTransform;

    void Start()
    {
        _parentTransform = GetComponentInParent<Transform>();
    }

    void FixedUpdate()
    {
        _rightHorizontal = Input.GetAxis("Right Analog Horizontal");
        _rightVertical = Input.GetAxis("Right Analog Vertical");

        if (DroneMoveController.Get_isOn())
        {
            if ((0.3 < _rightVertical) || (_rightVertical < -0.3) || (0.3 < _rightHorizontal) || (_rightHorizontal < -0.3))
            {
                if ((-0.3 > _rightVertical) && (_rightHorizontal < 0.3) && (_rightHorizontal > -0.3)) //DO GORY
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x + 25f, transform.eulerAngles.y, transform.localRotation.z), 3f * Time.deltaTime);

                if ((0.3 < _rightVertical) && (_rightHorizontal < 0.3) && (_rightHorizontal > -0.3)) //W DOL
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x - 25f, transform.eulerAngles.y, transform.localRotation.z), 3f * Time.deltaTime);

                if ((-0.3 > _rightHorizontal) && (_rightVertical < 0.3) && (_rightVertical > -0.3)) //W LEWO
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x, transform.eulerAngles.y, transform.localRotation.z + 25f), 3f * Time.deltaTime);

                if ((0.3 < _rightHorizontal) && (_rightVertical < 0.3) && (_rightVertical > -0.3))   //W PRAWO
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x, transform.eulerAngles.y, transform.localRotation.z - 25f), 3f * Time.deltaTime);

                if ((_rightHorizontal) > 0.3 && (_rightVertical < -0.3))    // GORNE PRAWO
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x + 25f, transform.eulerAngles.y, transform.localRotation.z - 25), 3f * Time.deltaTime);
                }
                if ((_rightHorizontal) < -0.3 && (_rightVertical < -0.3)) // GORNE LEWO
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x + 25f, transform.eulerAngles.y, transform.localRotation.z + 25), 3f * Time.deltaTime);
                }
                if ((_rightHorizontal) > 0.3 && (_rightVertical > 0.3)) //DOLNE PRAWO
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x - 25f, transform.eulerAngles.y, transform.localRotation.z - 25), 3f * Time.deltaTime);
                }
                if ((_rightHorizontal) < -0.3 && (_rightVertical > 0.3)) // DOLNE LEWO
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x - 25f, transform.eulerAngles.y, transform.localRotation.z + 25), 3f * Time.deltaTime);
                }
            }
            else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.localRotation.x, _parentTransform.eulerAngles.y, transform.localRotation.z), 3f * Time.deltaTime);
            }
        }
    }
}
