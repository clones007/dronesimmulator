using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAutoPilot : MonoBehaviour
{
    private float _currentDistance;

    private void Awake()
    {
        DroneMoveController.Set__isAutopilotOn(false);
    }

    void FixedUpdate()
    {
        _currentDistance = Vector3.Distance(FindClosestChargePoint(transform).transform.position, transform.position);

        if (DroneMoveController.Get__isAutopilotOn())
            GoToCharger();
        
        if (_currentDistance > DroneBatteryController.GetMaxDistance()-1.0f)
            DroneMoveController.Set__isAutopilotOn(true);
    }
    private void GoToCharger()
    {
        Vector3 goToVector = (FindClosestChargePoint(transform).transform.position - transform.position);
        Vector3 newdir = Vector3.RotateTowards(transform.forward, goToVector, 3 * Time.deltaTime, 0.0f);

        if (transform.position != FindClosestChargePoint(transform).transform.position)
        {
            transform.position = Vector3.MoveTowards(transform.position, FindClosestChargePoint(transform).transform.position, Time.deltaTime * 4.0f);

            if (!DroneBatteryController.GetIsCharging())
            {
                Quaternion LookAtRotation = Quaternion.LookRotation(newdir);
                Quaternion LookAtRotationOnly_Y = Quaternion.Euler(transform.rotation.eulerAngles.x, LookAtRotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                transform.rotation = LookAtRotationOnly_Y;
            }
        } 
    }
    public static GameObject FindClosestChargePoint(Transform _from)
    {
        GameObject[] _chargingPoints;
        _chargingPoints = GameObject.FindGameObjectsWithTag("Charger");

        GameObject _closestPoint = null;
        float _distance = Mathf.Infinity;

        foreach (GameObject _chargePoint in _chargingPoints)
        {
            float _currentDistance = Vector3.Distance(_chargePoint.transform.position, _from.position);
            if (_currentDistance < _distance)
            {
                _closestPoint = _chargePoint;
                _distance = _currentDistance;
            }
        }
        return _closestPoint;
    }
}


