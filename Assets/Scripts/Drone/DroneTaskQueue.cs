using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DroneTaskQueue : MonoBehaviour
{
    public List<TaskEx> _listOfTasks;

    private int i = 0;

    private float _enterTime;

    [SerializeField] Text _taskText;
    [SerializeField] Transform _baseTransform;

    public bool _canFly;

    private void Start()
    {
        _canFly = true;
    }

    private void Update()
    {

         // CanFlyToNext();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (DroneMoveController.Get_isOn() && !DroneMoveController.Get__isAutopilotOn())
        {
            if (_listOfTasks[i]._done == false)
            {
                GoToTasK(_listOfTasks[i]._pointTransform);

                if (Vector3.Distance(transform.position, _listOfTasks[i]._pointTransform.position) < 2f && !_listOfTasks[i]._done)
                {
                    _taskText.enabled = true;
                    _enterTime += Time.deltaTime;
                    _taskText.text = "Time left to complete task:  "+ (_listOfTasks[i]._time -_enterTime).ToString();

                    if (_enterTime > _listOfTasks[i]._time)
                    {
                        DroneBatteryController.SetBatteryLevel(DroneBatteryController.GetBatteryLevel() - _listOfTasks[i]._battery);
                        _enterTime = 0;
                        _listOfTasks[i]._done = true;
                        _taskText.enabled = false;
                    }
                }
            }

            if (_listOfTasks[i]._done && i < _listOfTasks.Count-1)
            {
                if (CanFlyToNext())
                {
                    i++;
                    _listOfTasks[i]._done = false;
                } 
                else
                {
                    DroneMoveController.Set__isAutopilotOn(true);
                }
            }
            else
            {
                GoToTasK(_baseTransform);
            }
        }
    }
    private void GoToTasK(Transform goToPoint)
    {
        Debug.Log("GOTO:   " +goToPoint);
        //Vector3 goToVector = (_listOfTasks[i]._pointTransform.position - transform.position);
        Vector3 goToVector = (goToPoint.position - transform.position);
        Vector3 newdir = Vector3.RotateTowards(transform.forward, goToVector, 3 * Time.deltaTime, 0.0f);

        Quaternion LookAtRotation = Quaternion.LookRotation(newdir);
        Quaternion LookAtRotationOnly_Y = Quaternion.Euler(transform.rotation.eulerAngles.x, LookAtRotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        transform.rotation = LookAtRotationOnly_Y;

        if (Vector3.Distance(transform.position, goToPoint.position) < 2f)
        {
            DroneMoveController.FlyStraightDrag(2.5f);
        } 
        else
        { 
            DroneMoveController.FlyStraight(10.0f, goToVector.normalized); 
        }

    }

    private bool CanFlyToNext()
    {
        float _maxSpeed = 4.79f; ///PREDKOSC MAKSYMALNA

        float _currentBattery = DroneBatteryController.GetBatteryLevel(); ///AKTUALNY POZIOM BATERIII
       
        float _batteryAfterThisTask = _currentBattery - _listOfTasks[i]._battery; ///POZIOM BATERII PO WYKONANIU TEGO ZADANIA
       
        float _distanceLeft = _currentBattery * _maxSpeed; //POZOSTALY DYSTANS NA BATERII
      
        float _nextTaskBattery = _listOfTasks[i + 1]._battery; ///ZUZYCIE BATERII NA KOLEJNE ZADANIE
      
        float _nextTaskTime = _listOfTasks[i + 1]._time; ///CZAS NA KOLEJNE ZADANIE
      
        float _timeLeftAfterThisTask = _batteryAfterThisTask * 0.5f; //POZOSTALY CZAS PRACY PO WYKONANIU AKTUALNEGO ZADANIA
      
        float _distanceToThisTask = Vector3.Distance(_listOfTasks[i]._pointTransform.position, transform.position); //DYSTANS DO OBECNEGO
       
        float _distanceLeftAfterThisTask = _maxSpeed * _timeLeftAfterThisTask - _distanceToThisTask; //DYSTANS POZOSTALY PO WYKONANIU ZADANIA
      
        float _distanceFromTaskToNextTask = Vector3.Distance(_listOfTasks[i]._pointTransform.position, _listOfTasks[i + 1]._pointTransform.position); //DYSTANS OD AKTUANEGO ZADANIA DO KOLEJNEGO
     
        float _distanceFromNextTaskToClosestCharger = Vector3.Distance(_listOfTasks[i + 1]._pointTransform.position, DroneAutoPilot.FindClosestChargePoint(_listOfTasks[i + 1]._pointTransform).transform.position); //DYSTANS DO LADOWARKI PO NASTEPNYM TASKU

        float _distanceLostInNextTask = (_nextTaskTime * _maxSpeed) + (_nextTaskTime * _maxSpeed); //DYSTANS STRACONY W TRAKCIE REALIZACJI NASTEPNEGO ZADANIA

        float _minusDis = _distanceLostInNextTask + _distanceFromTaskToNextTask; //DYSSTANS DO CELU I NA MIEJSCU

        float _distanceLeftAfterNextTask = _distanceLeft - _minusDis; //DYSTANS PO WYKONANIU KOLEJNEGO ZADANIA
        
        if ((_distanceLeftAfterNextTask > 0) && _distanceLeftAfterThisTask < _distanceFromNextTaskToClosestCharger || _distanceLeftAfterNextTask < 0)// || _distanceLeftAfterNextTask < 0))
        {
            _canFly = false;
        }
        else
        {
            _canFly = true;
        }
        return _canFly;
    }
}
[System.Serializable]

public class TaskEx
{
    public float _time;
    public float _battery;
    public Transform _pointTransform;
    public bool _done;
}