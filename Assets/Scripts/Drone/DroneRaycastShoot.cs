using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneRaycastShoot : MonoBehaviour
{
    private float _nextFireR, _nextFireL;
    private float _fireRate = 0.4f;
    private float _rightTrigger, _leftTrigger;

    [SerializeField] Transform _gunEndR;
    [SerializeField] Transform _gunEndL;

    [SerializeField] GameObject _bullet;

    [SerializeField] AudioSource _shootSound;

    [SerializeField] ParticleSystem _beam;
    [SerializeField] ParticleSystem _beam2;

    void FixedUpdate()
    {
        _rightTrigger = Input.GetAxis("ShootR");
        _leftTrigger = Input.GetAxis("ShootL");

        if ((_rightTrigger > 0) && Time.time > _nextFireR)
        {
            ShootR();
            _beam.Play();
            _shootSound.Play();
        }

        if ((_leftTrigger > 0) && Time.time > _nextFireL)
        {
            ShootL();
            _shootSound.Play();
            _beam2.Play();
        }
    }

    private void ShootR()
    {
        _nextFireR = Time.time + _fireRate;
        SpawnBulletR();
    }

    private void ShootL()
    {
        _nextFireL = Time.time + _fireRate;
        SpawnBulletL();
    }

    private void SpawnBulletR()
    {
        GameObject vfx;
        vfx = Instantiate(_bullet, _gunEndR.transform.position, transform.rotation);
    }

    private void SpawnBulletL()
    {
        GameObject vfx;
        vfx = Instantiate(_bullet, _gunEndL.transform.position, transform.rotation);
    }
}
