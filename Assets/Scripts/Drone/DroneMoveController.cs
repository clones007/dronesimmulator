using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DroneMoveController : MonoBehaviour
{
    private float _leftHorizontal, _leftVertical;
    private float _rightHorizontal, _rightVertical;
    private static float _moveSpeed = 0.05f;
    private static bool _isOn;
    private static bool _isAutopilotOn;

    private float _forceMultiplier = 10;

    public bool isOn;
    public bool isAutopilot;
    private float _maxSpeed;

    private static Rigidbody _droneRigidbody;
    private static Transform _droneTransform;

    public float _droneSpeed;

    public static void SetDroneRotation(float x, float y, float z)
    {
        _droneTransform.rotation = Quaternion.Euler(x, y, z);
    }

    public static void SetMoveSpeed(float value)
    {
        _moveSpeed = value;
    }
    public static float GetMoveSpeed()
    {
        return _moveSpeed;
    }

    public static void Set__isAutopilotOn(bool value)
    {
        _isAutopilotOn = value;
    }

    public static bool Get__isAutopilotOn()
    {
        return _isAutopilotOn;
    }

    public static void Set_isOn(bool value)
    {
        _isOn = value;
    }

    public static bool Get_isOn()
    {
        return _isOn;
    }

    public static void ChangeGravity()
    {
        _droneRigidbody.useGravity = !_droneRigidbody.useGravity;
    }

    public static bool GetUseGravity()
    {
        return _droneRigidbody.useGravity;
    }

    private void Start()
    {
        Set_isOn(false);
        Set__isAutopilotOn(false);

        _droneRigidbody = GetComponent<Rigidbody>();
        _droneTransform = GetComponent<Transform>();
        _droneRigidbody.useGravity = true;

        _maxSpeed = 100.0f;
    }

    public void Update()
    {
        if (Input.GetButtonDown("OnOffDrone"))
        {
            ////////////FUNKCJA ISSTARTING
            if (!Get__isAutopilotOn())
            {
                Set_isOn(!_isOn);
                ChangeGravity();
            }

            if (_droneTransform.rotation.x != 0 && _droneTransform.rotation.z != 0 && Get_isOn() && _droneTransform.position.y > 2)
            {
                SetDroneRotation(0, transform.rotation.y, 0);
            }

        }
        _droneSpeed = _droneRigidbody.velocity.magnitude;

        if (_droneRigidbody.velocity.magnitude > _maxSpeed)
            _droneRigidbody.velocity = _droneRigidbody.velocity.normalized * _maxSpeed;
    }


    void FixedUpdate()
    {
        isAutopilot = Get__isAutopilotOn();
        isOn = Get_isOn();

        _leftHorizontal = Input.GetAxis("Left Analog Horizontal");
        _leftVertical = Input.GetAxis("Left Analog Vertical");
        _rightHorizontal = Input.GetAxis("Right Analog Horizontal");
        _rightVertical = Input.GetAxis("Right Analog Vertical");

        if (_isOn)
        {
            if (!Get__isAutopilotOn())
            {
                //ControllerInput();
                ControllerInputRigidbody();
            }
        }
    }

    public void ControllerInputRigidbody()
    {
        if ((0.3 < _leftVertical) || (_leftVertical < -0.3) || (0.3 < _rightVertical) || (_rightVertical < -0.3) || (0.3 < _rightHorizontal) || (_rightHorizontal < -0.3))
        {
            if (0.3 < _leftVertical)
            {
                _droneRigidbody.AddForce(-transform.up * _forceMultiplier);
            }
            if (_leftVertical < -0.3)
            {
                _droneRigidbody.AddForce(transform.up * _forceMultiplier);
            }
            if (0.3 < _rightVertical)
            {
                _droneRigidbody.AddForce(-transform.forward * _forceMultiplier);
            }
            if (_rightVertical < -0.3)
            {
                _droneRigidbody.AddForce(transform.forward * _forceMultiplier);
            }
            if (0.3 < _rightHorizontal)
            {
                _droneRigidbody.AddForce(transform.right * _forceMultiplier);
            }
            if (_rightHorizontal < -0.3)
            {
                _droneRigidbody.AddForce(-transform.right * _forceMultiplier);
            }
        }
        else
        {
            _droneRigidbody.drag = 2.0f;
        }

        if ((0.3 < _leftHorizontal) || (_leftHorizontal < -0.3))
        {
            transform.Rotate(0, _leftHorizontal, 0);
        }
    }


    public void ControllerInput()
    {

            if ((0.3 < _leftHorizontal) || (_leftHorizontal < -0.3))
                transform.Rotate(0, _leftHorizontal * 0.5f, 0);

            if ((0.3 < _rightVertical) || (_rightVertical < -0.3))
                transform.Translate(0, 0, -_rightVertical * _moveSpeed);

            if ((0.3 < _leftVertical) || (_leftVertical < -0.3))
                transform.Translate(0, -_leftVertical * _moveSpeed, 0);

            if ((0.3 < _rightHorizontal) || (_rightHorizontal < -0.3))
                transform.Translate(_rightHorizontal * _moveSpeed, 0, 0);
    }

    public void KeyboardInput()
    {
        if (Input.GetKey(KeyCode.O))
        {
            _droneRigidbody.AddForce(transform.up * 4);
        }
        else
        {
            _droneRigidbody.drag = 2f;
        }

        if (Input.GetKey(KeyCode.I))
        {
            _droneRigidbody.AddForce(transform.forward * 4);
        }
        else
        {
            _droneRigidbody.drag = 2f;
        }

        if (Input.GetKey(KeyCode.U))
        {
            _droneRigidbody.AddForce(transform.right * 4);
        }
        else
        {
            _droneRigidbody.drag = 2f;
        }


        if ((Input.GetKey(KeyCode.Y) && (Input.GetKey(KeyCode.T))))
        {
            _droneRigidbody.AddForce(transform.right * 2f);
            _droneRigidbody.AddForce(transform.forward * 2f);
        }
    }

    public static void FlyStraight(float _forceMulti, Vector3 _trans)
    {
        _droneRigidbody.AddForce(_trans * _forceMulti);
    }

    public static void FlyStraightDrag(float _value)
    {
        _droneRigidbody.drag = _value;
    }
}
