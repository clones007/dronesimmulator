using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DroneBatteryBar : MonoBehaviour
{
    private Image _batteryBar;
    [SerializeField] Text _batteryValue;

    void Start()
    {
        _batteryBar = GetComponent<Image>();
    }

    private void Update()
    {
        float _batteryLevel = DroneBatteryController.GetBatteryLevel();
        _batteryBar.fillAmount = _batteryLevel/100;
        _batteryValue.text = _batteryLevel.ToString("F")+"%";
    }
}
