using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBatteryController : MonoBehaviour
{
    private static float _batteryLevel;
    private static float _time;

    private float _timeRate = 0.01f;
    private float _nextCheck = 0.01f;
    private float _baterryAddAndLose = 0.01f;

    private static float _batteryTimeLeft;
    private static bool _isCharging;
    private static float _maxDis;
    private float _loseBatteryForSec;

    public static void SetMaxDistance(float value)
    {
        _maxDis = value;
    }
    public static float GetMaxDistance()
    {
        return _maxDis;
    }
    public static void SetIsCharging(bool value)
    {
        _isCharging = value;
    }
    public static bool GetIsCharging()
    {
       return _isCharging;
    }

    // Start is called before the first frame update
    void Awake()
    {
        SetBatteryLevel(100f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _time = Time.realtimeSinceStartup;
        _batteryLevel = Mathf.Clamp(_batteryLevel, 0, 100);

        if (_time > _nextCheck)
        {
            if (DroneMoveController.Get_isOn())
            {
                if (GetBatteryLevel() >= 0.01f)
                    SetBatteryLevel(GetBatteryLevel() - _baterryAddAndLose); //0.01
            }

            if (_isCharging)
            {
                if (GetBatteryLevel() <= 99.99f)
                    SetBatteryLevel(GetBatteryLevel() + _baterryAddAndLose + 0.05f); //0.01 + 0.02

                if (DroneMoveController.Get__isAutopilotOn())
                {
                    DroneMoveController.Set_isOn(false);
                    if (!DroneMoveController.GetUseGravity())
                    {
                        DroneMoveController.ChangeGravity();
                    }
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), 3f * Time.deltaTime);
                   // DroneMoveController.Set__isAutopilotOn(false);
                }
            }
            _nextCheck = _time + _timeRate;
        }

        if ((DroneMoveController.Get__isAutopilotOn()) && (GetBatteryLevel() > 90.0f))
        {
            DroneMoveController.Set__isAutopilotOn(false);
        }

        _loseBatteryForSec = ((1 / _timeRate) * _baterryAddAndLose);
        SetBatteryLeftTime(GetBatteryLevel() / _loseBatteryForSec);
        SetMaxDistance(DroneMoveController.GetMoveSpeed() * GetBatteryLeftTime()*100);
    }

    public static void SetBatteryLevel(float value)
    {
        _batteryLevel = value;
    }

    public static void SetBatteryLeftTime(float value)
    {
        _batteryTimeLeft = value;
    }

    public static float GetBatteryLeftTime()
    {
        return _batteryTimeLeft;
    }

    public static float GetBatteryLevel()
    {
        return _batteryLevel;
    }

    public static float GetRealTime()
    {
        return _time;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "ChargerTrigger")
            _isCharging = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "ChargerTrigger")
            _isCharging = false;
    }
}

