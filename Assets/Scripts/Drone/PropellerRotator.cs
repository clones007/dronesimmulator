using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class PropellerRotator : MonoBehaviour
{
    private float _rotateSpeed;

    private void Awake()
    {
        _rotateSpeed = 0;
    }

    void FixedUpdate()
    {
        if (DroneMoveController.Get_isOn())
        {
            if (_rotateSpeed < 2)
            {
                _rotateSpeed += 0.5f * Time.deltaTime;
            }
        } 
        else
        {
            if (_rotateSpeed > 0)
            {
                _rotateSpeed -= 0.5f * Time.deltaTime;
            } 
            else
            {
                _rotateSpeed = 0;
            }
        }
        transform.Rotate(transform.parent.up, _rotateSpeed * Mathf.Rad2Deg, Space.World);
    }
}
